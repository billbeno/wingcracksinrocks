pomodoro timer in vim


(i) Convert a time domain signalf(t) into the frequencydomain.
(ii) Derive the amplitude and phase angle φω of eachfrequency component.
(iii) Calculate the phase velocity Cω of each frequencycomponent (from [4] or [5]).
(iv) For each frequency component, calculate the phaseshift from the position at which the signal is recorded to some other point at an axial distance z from the recording position (z measured positive in the direction ofpropagation of the wave).
The phase shift φ′ω in radians,is given by φ′ω=(C0Cω−1)ωzC0 where ω is  the  angular  frequency of the component  inquestion.
(v)  Convert  the  signal  back  into  the  time  domain  using  acorrected phase angle:φωcorr=φω−φ′ω.
-- Tyas and Pope, P-C dispersion effcts, 2004, Meas. Sci. Technol.
